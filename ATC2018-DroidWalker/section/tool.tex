To achieve automation, Paladin uses a model-based approach to explore as many app behaviors as possible. The key design of Paladin to achieve reproducibility is an app state model. Our philosophy is that the functionality provided for and seen by the users keep consistency under different environments. Therefore, we use the GUI structure to identify app states, and the events triggering the GUI switches as the state transfers. Every state in the model represents a specific kind of behaviors which provide the same functionality to users but may differ in the exhibited data. To achieve practical usage, we implement Paladin at the framework level of Android system based on Xposed~\cite{xposed}, thus not having to instrument the applications. Although the framework-level implementation affects the deployment convenience compared with tools relying just on Android Debug Bridge tool (ADB) provided by the Android SDK, we are able to collect all the runtime information of applications via reflection mechanism, enabling further extension and flexibility of our system.

Figure~\ref{fig:workflow} shows the architecture of Paladin. First, Paladin explores the app behaviors, constructing a state model. Second, developers can select a target state from the model, and Paladin traverses the model in a breadth first order to enumerate every possible event sequences to reach the target state, forming the generated test case. Third, executing the test case is just to trigger the events in the sequences one by one. Last but not the least, Paladin has an executor interface to retrieve UI information and to generate flexible and dynamic-adaptive events. The rest of this section describes details of the design and implementation of each component.

\begin{figure}[t!]
\centering
  \includegraphics[width=0.5\textwidth]{./picture/tool}
  \vspace{-3em}
  \caption{Architecture of Paladin.}~\label{fig:workflow}
  \vspace{-2em}
\end{figure}

\subsection{State Identifier}
Paladin uses finite-state machine to model the GUI behaviors. We propose to use the UI structure to identify states. This design is inspired by an empirical fact that regardless of the different assigned parameters, the UIs rendered by the same app behavior always share the same structure, and different app behaviors are usually different in the structure. For example, the pages of different restaurant details have the same structure, but the detail page and list page have obvious differences in the UI structure. In this way, we bring in some tolerance to the UI difference. Since the same UI has the same structures across device configurations, we could solve the fragmentation issue with state identifier.

\begin{algorithm}
\footnotesize
    \caption{Computing structure hash.}
    \label{algo:structhash}
    \SetAlgoLined
    \KwIn{View $r$}
    \KwOut{Structure Hash $h$}
        \textbf{function}~TreeHash($r$)\\
        $str \leftarrow r.viewTag$\\
        \If{$r.InstanceOfWebview()$} {
                $r.setChildren \leftarrow r.parseHTML()$\\
        }
        \If{$r.hashChildren()$} {
            $children \leftarrow r.getChildren()$\\
            \ForEach{$c \in children$} {
                $c.hash \leftarrow TreeHash(c)$\\
            }
            $children \leftarrow SortByHash(r.getChildren())$\\
            \If{$r.InstanceOfListview()$} {
                $children \leftarrow children.unique()$\\
            }
            \ForEach{$c \in children$} {
                $str \leftarrow str + c.hash$
            }
        }
    \Return{$hash(str)$}
\end{algorithm}

In Android, all the UI widgets are organized in a structure of hierarchy tree. To rapidly compare two UI's structural difference, we design a recursive bottom-up algorithm~\ref{algo:structhash} to encode the structure into a hash value, and use the hash to distinguish different UI's structure. The algorithm is recursive with a view $r$ as input. If $r$ does not have children, the result is only the string hash of $r$'s view tag (Line 2). If $r$ has children (Line 6), then we use the algorithm to calculate all the hash values of its children recursively (Lines 8-10). Then, we sort $r$'s children based on their hash values to ensure the consistency of the structure hash, because a view's children do not keep the same order every time (Line 11). In many cases, there is a list-view or recycle-view that contain multiple views with the same structure. Under such circumstances, we should count the items with the same structure hash by only once (Line 13). Next, we add each children's hash together with the view tag, forming a new string (Line 16), and finally return the string hash (Line 19). Given the root view of the tree, the algorithm returns a structure hash of the view tree. The hash can be used as an identifier of a UI state.

This recursive structural encoding method can be extended to Web elements. Since there is a plenty of hybrid apps which use HTML in WebView to display GUI, we also incorporate the Web elements inside WebView component into the hierarchical tree (Line 4) rather than treat the WebView as a leaf node. As a result, our system is applicable for hybrid apps.

\subsection{DFS Explorer}

Similar to the previous model-based app-exploration tools, we use a depth first strategy to explore the app. For each state, we extract all the potential widgets that have event listeners, and then systematically generate the event of each widget. Next, we check whether the event brings the app to a new state, by comparing its structural hash with all the other states in the state model. If a new state is identified, we recursively apply the exploring algorithm on the new state. When the exploration on this state terminates, we execute a backtrack method to the previous state.

%\begin{algorithm}
%\footnotesize
%    \caption{DFS Explore}
%    \label{algo:explore}
%    \SetAlgoLined
%    \KwIn{Current UI Tree $tree$, State Model $model$}
%        \textbf{function}~DFS\_Explore($A$, $model$)\\
%        \ForEach{$widget \in tree.getClickableWidget()$} {
%            $GenerateEvent(widget)$\\
%            $currentTree \leftarrow BuildTree()$\\
%            \If{$it is a UI state not yet explored$} {
%                $model.add(currentView)$\\
%                $DFS\_Explore(currentTree, model)$\\
%                $backTrack(tree, currentTree)$\\
%            }
%        }
%\end{algorithm}

Although depth-first search has been widely adopted by various app-exploration tools, there is an efficiency problem related to backtracking, i.e. returning to the previous state and continuing to execute the remaining events of that state. The challenge is because the \emph{goBack} method provided by Android system only goes back to the previous activity rather than UI state. Therefore, a common strategy used by existing tools is to restart the app, and re-send the events from the initial state. This strategy can somehow achieve backtracking, but it faces accuracy and efficiency problem. %On one hand, due to the dynamic nature of Android apps and the difficulty of accurately replaying, sometimes the reinstall method may not successfully return to the correct state. On the other hand, frequently restarting the app may cost dramatic amount of time, which compromises the exploration efficiency. Especially in the circumstance when we use the fine-grained UI states, a test case may include hundreds of states, and considering a precise UI replay always need to wait for some time for the UI state to stabilize.

To achieve the precise backtracking with minimal time cost, we propose to take the advantage of Android's intent mechanism. We record the intent information when transiting between activities at runtime. Whenever the executor backtracks to a previous state, it sends the intent of the target state's activity, and check if the current state is in the stack. If so, the executor will resend the UI traces in order to get the target state.

%\begin{algorithm}
%\footnotesize
%    \caption{backTrack}
%    \label{algo:back}
%    \SetAlgoLined
%    \KwIn{Target State $T$, Current State $S$}
%        \textbf{function}~backTrack($T$, $S$)\\
%        $stack \leftarrow T.UIStack$\\
%        \If{$stack.activity \ne getCurrentActivity()$} {
%            $SendIntent(stack.intent)$\\
%        }
%        $S \leftarrow getCurrentState$\\
%        \ForEach{$state \in stack.states$} {
%            \If{$Similarity(S, state) > threshold$} {
%                $stack.ReplayTracesFrom(state)$\\
%            }
%        }
%\end{algorithm}


\subsection{Target State Reproducer}
After the exploration, a state model is constructed. Developers can select any state to generate a test case. Since the model records the necessary event information, such as which widget should be executed and which action should be sent, the event can be triggered between arbitrary two states. Therefore, the key idea of generating reproducible test cases is to find the potential paths from the entry state of the model to the target state. This can be done by a standard bread-first search. Started from the target state, the algorithm enumerates every potential state path, and sorts them by their length. These paths are a sequence of generated test cases.

\subsection{Test Executor}
After a sequence of tests are generated, the executor will launch these test cases one by one, by extracting the states and generate the events to each states. The executor will check at each step whether the correct state is reached, ensuring the authenticity of the test cases. Although the assigned hash value can distinguish nodes with different structure, its ability to tolerate app change is quite low. In the reproducing phase, we need a more adaptive approach to identify current state to address the flakiness issue.

Due to the dynamic nature of android app, the UI may sometimes change even when the same sequence of events are executed, and a trivial UI change may result in a totally different structural hash value. For example, when a test case to explore news page is generated, a notification about the news update may appear on top of the screen. Since it is only a trivial UI change and does not influence the majority of the other functions, we should tolerate this difference and continue executing. Therefore, in order to make our approach more scalable, it is necessary to incorporate a structural similarity criteria. Only when a UI's similarity difference to the previous state is below the pre-defined threshold should we assign a new model state. The algorithm~\ref{algo:diff} accept two arbitrary view node and a threshold as inputs.

\begin{algorithm}
\footnotesize
    \label{algo:diff}
    \SetAlgoLined
    \KwIn{View $s$, View $t$, threshold $thd$}
    \KwOut{Structual Similarity $sim$}
        \textbf{function}~Similarity($s$, $t$)\\
        \If{$s.hash = t.hash$} {
            \Return{$1$}\\
        }
        \If{$s.tag \neq t.tag$} {
            \Return{$0$}\\
        }
        $hits \leftarrow 1$\\
        \ForEach{$sc \in s.getChildren()$} {
            \ForEach{$tc \in t.getChildren()$} {
                $tmp \leftarrow Difference($sc$, $tc$)$\\
                \If{$tmp > threshold$} {
                    $hits \leftarrow tmp * tc.count$\\
                    $break$\\
                }
            }
        }
        \Return{$2 * hits / (s.count + c.count)$}
    \caption{Structual Similarity between two hierarchical UI trees.}
\end{algorithm}

If the two UI nodes share the same hash value, their structure are the same, so the similarity equals 1 (Line 2). Otherwise, there must be some differences between them. So we enumerate the children of these two nodes, and calculate the similarity of these children (Line 9-17). To reduce complexity, we will stop traversing if a children pair reach the threshold (Line 12). The similarity is the shared nodes divided by the total nodes (Line 18).

\subsection{Executor Interface}
The executor interface connects with the app execution environment. It provides necessary functions for the higher level system modules. In our implementation, we build the executor interface at the framework level based on Xposed. We modify the process creator of Android to inject the logic of executor into the same process space so that the executor can use reflection mechanism to acquire all the runtime information of the application. For the Web contents in WebViews, we use Chrome Remote Debugging protocol to inject a JavaScript implementation of the executor into the Web page.

The executor interface supports two major functions. On one hand, the interface is used to retrieve the information, such as extracting structural UI information, collecting the event listens, and recording the intent information. On the other hand, the interface triggers events when notified which widget should be executed by a certain action.
