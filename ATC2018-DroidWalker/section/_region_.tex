\message{ !name(Usage_scenarios.tex)}
\message{ !name(Usage_scenarios.tex) !offset(-2) }
\begin{figure}[t!]
\centering
  \includegraphics[width=0.45\textwidth]{./picture/test}
  \caption{Boxplot of the line coverage result across 20 benchmark apps.}~\label{fig:res}
\end{figure}

\begin{table*}[t!]
\scriptsize
\caption{Activity and method coverage result across five commercial app experiment, comparing with Monkey.}
\label{tab:seven_table}
\centering
\begin{tabular}[t]{|c|c|c|c|c|c|c|}
    \hline
    \textbf{App} & \textbf{Category} & \textbf{\tabincell{c}{Total\\ Activities}} & \textbf{\tabincell{c}{Activity Coverage\\ of DroidWalker}} & \textbf{\tabincell{c}{Activity Coverage\\ of Monkey}} & \textbf{\tabincell{c}{Method Coverage\\ of DroidWalker}} & \textbf{\tabincell{c}{Method Coverage\\ of Monkey}} \\ \hline
    \textbf{accuweather} & weather  &    37    &    11    &   8   &   2314     &     1786 \\
    \hline
    \textbf{lionbattery} & tools    &    100   &    29    &   23  &   2034     &     1317 \\
    \hline
    \textbf{iconology} & comics     &    41    &    22    &   18  &   3039     &     2253 \\
    \hline
    \textbf{fibit} & Health         &    92    &    28    &   21  &   6702     &     4927 \\
    \hline
    \textbf{vyapar} & Business      &    88    &    39    &   36   &  2543     &     1759\\
    \hline
\end{tabular}
\end{table*}

DroidWalker is designed to automatically generate reproducible test cases for Android apps. The current implementation offers a command-line tool and a visualized webpage. This section demonstrates three main usage scenarios of DroidWalker: (1) automate app exploration, (2) customize detection functions (3) reproduce target state.

\subsection{Automate App Exploration}

The first usage scenario is using DroidWalker to explore app under test automatically. Users should follow steps below:
\begin{enumerate}
\item Prepare the apk of the app to be explored. Then run a python script to repackage and build the app. The script can be configured with command-line arguments below, the repackaged project is located in the <lists> folder under the working directory:

\begin{tiny}
\begin{lstlisting}[ language=C]
python buildAndResigned.py APK\_Address
[--ip<address>] [--port<port>] [--output<graph|report|both>]
\end{lstlisting}
\end{tiny}


* [--ip<address> --port<port>]: optional, specify ip and port of the server used to upload exploring output, default to be localhost:5000
* [--output<graph|report|both>]:  optional, specify whether to output coverage report and the content of output, including a graph which is a visualization of the constructed GUI model, a progressive coverage plot over time. The default argument is both.

The output information will be visualized on the webpage. If the source code of the app under test is available, users can choose to instrument the app with Emma to get a more detailed coverage report or the coverage information will be generated using logcat and tracedump offered by Android SDK.

\item Start the server of visualized webpage using the command line: \emph{python server.py}, the console will output the specific listening port of the server.


\item Open Android emulator or connect with Android devices, then change directory to <lists/TestAPP/app/build/outputs/apk>. Start exploring the app using the command line: \emph{./runTest.sh}



\item Open a browser and enter the listening address of the server, and start monitoring the exploring output.
The webpage consists of a tab panel. By clicking the tab, user can switch between graph and coverage report. The webpage also demonstrates snapshots of the app, each of which corresponds to a specific node in the graph.

\end{enumerate}

To assess the efficiency of DroidWalker when applied to automate app exploration, we conduct two experiments to address the following two questions: first, whether our tool is superior than other automated exploring tools on a standard benchmark. Second, whether the tool can be applied to highly complex commercial apps.

For the first question, in order to be comparable, we use a set of mobile app benchmarks collected in the work done by Choudhary \textit{et al.}~\cite{Choudhary2016Automated}, and concerned both the code coverage and the coverage time as criteria to evaluate our tool.

The experiment setup is as follows:


1) Experiment 1: We ran our tool in the same setting as Choudhary \textit{et al.}~\cite{Choudhary2016Automated}, using the Ubuntu virtual machine configured with 2 cores and 6GB RAM, and an android emulator was configured with 4GB RAM. Each tool was allowed to run for 1 hour on each benchmark app. We chose Android SDK version 18 (Jelly Bean) to evaluate our tool based on the compatibility of most benchmark apps. To avoid unexpected side effects between tools and app, the emulator is destroyed after finishing each run. We repeated exploring each app for 3 times. From Fig.~\ref{fig:res}, we can see that line coverage of our tool is higher than those of the state-of-art tools, especially the model-based tools, such as A3E,GUIRipper and PUMA.

Although exploring a common set of apps is necessary for comparison, the fact that most apps in benchmarks are small and has simple logics indicates the result could not represent the performance of exploring tool on modern commercial apps. Thus, we select ten top apps from different categories on the Google Play and use the same two criteria to evaluate those apps. The experiment setup is as follows:

2) Experiment 2: Five top apps in Google Play are chosen to be tested. Because the applicaitons are more complicated, we chose Genymotion, an optimized android emulator to perform the experiment. The emulator settings are identical to Experiment 1. From Table~\ref{tab:seven_table}, we can see that our tool outperforms monkey even on complex commercial apps.




\subsection{Customize Detection Function}

The second usage scenario is using DroidWalker to customize detection function which can be called during exploration. User can write their own class under <config> directory. The class should be inherited from \texttt{Customize} class and override three methods--input, output and operate. Add the following argument while repackaging and building the app:
$*[--configure-class <Customized\_class>]$

The Customize class contains an outputContent field. The input method is designed to accept existing data in string format provided by users. The operate method is designed to be the central customized detection function. The output method is designed to deal with output, which is default to be sent to the server of visualized webpage.

Customized class will be instantiated inside the testing program and called during exploration. User can customize their own class to detect advertisements, code defects or code vulnerability. To demostrate customizing testing function, we write an exception collection class as example. The configuration of Android emulator is the same as experiment 2. The class example is as follows:

\begin{tiny}
\begin{lstlisting}[language=java, label = {code:except}]
public class ExceptionCollector extends Customize {
  public void operate(){
    Process process = Runtime.getRuntime().exec("logcat -d PACKAGE_NAME:I");
    BufferedReader bufferedReader = new BufferedReader(
        new InputStreamReader(process.getInputStream()));
    StringBuilder log = new StringBuilder();
    String line = "";
    Boolean flag = false, has_except = false;
    while ((line = bufferedReader.readLine()) != null) {
      if (line.contains("Exception")){
        flag = has_except = true;
        log.append(line + "\n");
      }else if(flag){
        if (line.contains("at")) log.append(line + "\n");
        else flag = false;
      }
    }
    Runtime.getRuntime().exec("logcat -c");
    if (has_except) outputContent = log.toString();
  }
}
\end{lstlisting}
\end{tiny}

then repackage and build the project using:

\begin{tiny}
\begin{lstlisting}[language=python]
python buildAndResigned.py APK --configure-class ExceptionCollector
\end{lstlisting}
\end{tiny}


We applied our tool to Fitbit, a top health care appliaction on Google Play. The tool recorded two exceptions during exploration. The first one is java.lang.UnsupportedOperationException, which suggests the testing program attempted to open a session that has a pending request to the user's FaceBook account. The second one is java.io.FileNotFoundException, which was raised when the testing program attempted to take pictures or select photo from album for profile. The trace information is as follows:


\begin{tiny}
\begin{lstlisting}[language=python]
failed to get https://android-api.fitbit.com/1/user/5NZZKF/badges.json
from cache  java.io.FileNotFoundException:
/data/data/com.fitbit.FitbitMobile/cache/datacache/https%3A%2F%2Fandroid
-api.fitbit.com%2F1%2Fuser%2F5NZZKF%2Fbadges.json:
 open failed: ENOENT (No such file or directory)
    at libcore.io.IoBridge.open(IoBridge.java:409)
    at java.io.FileInputStream.<init>(FileInputStream.java:78)
    at com.fitbit.data.repo.q.a(SourceFile:71)
    at com.fitbit.serverinteraction.ServerGateway.b(SourceFile:811)
    at com.fitbit.serverinteraction.ServerGateway.a(SourceFile:780)
    at com.fitbit.serverinteraction.PublicAPI.a(SourceFile:352)
    at com.fitbit.serverinteraction.PublicAPI.b(SourceFile:2376)
    at com.fitbit.data.bl.BadgesBusinessLogic.a(SourceFile:171)
    at com.fitbit.profile.ui.badges.a.c(SourceFile:48)
    at com.fitbit.profile.ui.badges.a.b_(SourceFile:18)
    at com.fitbit.util.bd.loadInBackground(SourceFile:36)
     ...
\end{lstlisting}
\end{tiny}

\subsection{Reproduce Target State}
The third usage is using DroidWalker to reproduce target states. DroidWalker will number each UI status. User can relate UI number with its corresponding snapshot and customize UI number sequence. The app uses Breath-First-Search strategy to generate a sequence of test cases, which are sorted by their event length, and these test cases will be executed one by one.

Users can detect severe code errors when DroidWalker encounters a crash. After revising related code, user can enter the UI number causing the crash to check whether the error has been fixed. Users can also make use of the customized detection functions, repeating test cases and checking the detection results to comfirm app behaviors.

\begin{figure}
\begin{minipage}[t]{0.47\linewidth}
\centering
\includegraphics[width=1.55in]{./picture/045.png}
\caption{Target state selector.}
\label{fig:side:a}
\end{minipage}%
\begin{minipage}[t]{0.47\linewidth}
\centering
\includegraphics[width=1.8in]{./picture/047.png}
\caption{State model displayer.}
\label{fig:side:b}
\end{minipage}
\label{fig:side}
\end{figure}

For demonstratation, We use the tool to reproduce the java.io.FileNotFoundException exception recorded by the detector. On the webpage, We can find out the corresponding snapshot number is 6, as is shown in Fig.~\ref{fig:side:a}. Enter 6 in the input field below the graph and click \emph{send}.

The testing program successfully search the shortest path to the profile page, as is shown in Fig.~\ref{fig:side:b}, and start exploring again. Once the testing program click on \emph{Take Picture} or \emph{Select from other}, the exception repeated again.


\message{ !name(Usage_scenarios.tex) !offset(-174) }
