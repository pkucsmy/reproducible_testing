The key insight of Paladin is that the GUI rendering structure of a specific app page remains almost the same regardless of the changes of the contents and layouts when a test case is re-executed. Therefore, we use the complete structure of the GUI view tree to build a structure-preserving model for identifying desired app states and locating UI widgets precisely. We next describe the details of the fundamental design of state identification and UI widget location.

\subsection{Identifying App State}
In Android, all the UI widgets of a page are organized in a GUI view tree, similar to the DOM tree of a web page. We propose to use the rendering structure of the GUI view tree to identify app states. This design is inspired by an empirical fact that the GUIs rendered by the same app behavior always share the same structure, but different app behaviors usually have different structures. For example, the pages of different restaurant details have the same structure, but the detail page and the list page are obviously different in the GUI structures. Moreover, the page has the same GUI structure across device configurations, and thus the model can address the fragmentation issue.

In order to rapidly compare the rendering structures of GUIs and identify the differences, we design a recursive bottom-up algorithm~\ref{algo:structhash} that encodes the rendering structure into a hash value, and then use the hash values to distinguish different structures. The algorithm accepts a view node $r$ as input. First it set the hash value of the view node to 0. If $r$ has children nodes (Line 6), then we use the algorithm to calculate all the hash values of its children recursively (Lines 8-10). Then, the algorithm sorts $r$'s children based on their hash values to ensure the consistency of the structure hash value, since a view's children do not keep the same order every time (Line 11). Later, we sum up the children's hash value as  $r$'s hash and return it. If $r$ does not have children nodes, the result is only the hash value of $r$'s view tag and resource id(Line 17). Given the root of the view tree, the algorithm returns a hash value, which can be used as a state identifier.

\begin{algorithm}
\scriptsize
    \SetAlgoLined
    \KwIn{View $r$}
    \KwOut{Structure Hash $h$}
        \textbf{function}~TreeHash($r$)\\
        $r.hash \leftarrow 0$ \\
        \If{$r.InstanceOfWebview()$} {
                $r.setChildren \leftarrow r.parseHTML()$\\
        }
        \If{$r.hasChildren()$} {
            $children \leftarrow r.getChildren()$\\
            \ForEach{$c \in children$} {
                $c.hash \leftarrow TreeHash(c)$\\
            }
            $children \leftarrow SortByHash(r.getChildren())$\\
            \ForEach{$c \in children$} {
                $r.hash \leftarrow r.hash + c.hash$ \\
            }
            \Return{$r.hash$} \\
        }

    \Return{$hash(r.viewTag + r.resourceId)$}
    \caption{Structure hash of a view tree.}\label{algo:structhash}
\end{algorithm}

%In many cases, there is a list-view or recycle-view that contain multiple views with the same structure. Under such circumstances, we should count the items with the same structure hash by only once (Line 13). Next, we add each children's hash together with the view tag, forming a new string (Line 16), and

This encoding method can be extended to Web elements as well. Since there is a plenty of hybrid apps which use HTML in WebView to display GUIs, we also incorporate the Web elements inside WebView component into the view tree (Line 4) rather than treating the WebView as a leaf node. So our system is applicable for hybrid apps.

Due to the non-determinism of app behaviors, the GUI may change in certain parts when the same sequence of events are triggered, and a trivial GUI change may result in a totally different structural hash value. For example, when a test case to explore the news page is re-executed, a notification about the news update may appear on the top of the screen. Since it is only a trivial GUI change and does not influence the majority of the other functionality, we should tolerate this difference and continue executing. Therefore, in order to make the model adaptive to changes, we design a structural similarity criteria. Given a computed GUI state triggered by an event, only when the similarity difference between the GUI's state and our stored state is above a threshold, we would regard it a new state. Algorithm~\ref{algo:diff} shows how the similarity score is computed.

The algorithm accepts two view nodes and a threshold as inputs. If the two view nodes share the same hash value, their rendering structures are the same, so the similarity equals 1 (Lines 2-4). Otherwise, there must be some differences between them. So we enumerate the children of these two nodes, and compute the similarities of these children (Lines 9-17). To reduce the complexity, we will stop traversing if any pair of children nodes exceed the threshold (Lines 12-15). The similarity score is twice the number of nodes that are considered as the same divided by the total number of nodes including the two view nodes and their descendant (Line 18).

%\begin{algorithm}
%\footnotesize
%    \caption{DFS Explore}
%    \label{algo:explore}
%    \SetAlgoLined
%    \KwIn{Current UI Tree $tree$, State Model $model$}
%        \textbf{function}~DFS\_Explore($A$, $model$)\\
%        \ForEach{$widget \in tree.getClickableWidget()$} {
%            $GenerateEvent(widget)$\\
%            $currentTree \leftarrow BuildTree()$\\
%            \If{$it is a UI state not yet explored$} {
%                $model.add(currentView)$\\
%                $DFS\_Explore(currentTree, model)$\\
%                $backTrack(tree, currentTree)$\\
%            }
%        }
%\end{algorithm}



%\begin{algorithm}
%\footnotesize
%    \caption{backTrack}
%    \label{algo:back}
%    \SetAlgoLined
%    \KwIn{Target State $T$, Current State $S$}
%        \textbf{function}~backTrack($T$, $S$)\\
%        $stack \leftarrow T.UIStack$\\
%        \If{$stack.activity \ne getCurrentActivity()$} {
%            $SendIntent(stack.intent)$\\
%        }
%        $S \leftarrow getCurrentState$\\
%        \ForEach{$state \in stack.states$} {
%            \If{$Similarity(S, state) > threshold$} {
%                $stack.ReplayTracesFrom(state)$\\
%            }
%        }
%\end{algorithm}

\begin{algorithm}
\scriptsize
    \SetAlgoLined
    \KwIn{View $s$, View $t$, threshold $\tau$}
    \KwOut{Structural Similarity $sim$}
        \textbf{function}~Similarity($s$, $t$)\\
        \If{$s.hash = t.hash$} {
            \Return{$1$}\\
        }
	   $hits \leftarrow 0$\\

	\If{$s.tag = t.tag$} {
            $hits \leftarrow hits + 1$
        }

        \ForEach{$sc \in s.getChildren()$} {
            \ForEach{$tc \in t.getChildren()$} {
                $tmp \leftarrow Similarity(sc, tc)$\\
                \If{$tmp > \tau$} {
                    $hits \leftarrow hits + tmp * tc.count$\\
                    $break$\\
                }
            }
        }
        \Return{$2 * hits / (s.count + c.count)$}
\caption{Structural similarity of two view trees.}\label{algo:diff}
\end{algorithm}
\vspace{-1em}
\subsection{Locating UI Widget}
To ensure the reproducibility of test cases, we also need to precisely locate the UI widgets that trigger the desired events during test-case generation. Therefore, when the generated test case is re-executed, each event can be triggered at the desired UI widget. We summarize 3 conditions in Figure~\ref{fig:widgetLocation} to illustrate the challenges of precisely locating the UI widgets. Figure~\ref{fig:widgetLocation}(a) indicates a typical GUI view tree, on which leaf nodes represent the targeted widgets to trigger events and non-leaf nodes represent the intermediate layout elements. Supposed we want to precisely locate the leaf node with the dashed border line. Case 1, Figure~\ref{fig:widgetLocation}(b), shows the view tag changes on the black node (e.g. android.view.View in Android 5.1 is replaced by android.view.ViewGroup in Android 6.0+ instead). Case 2, Figure~\ref{fig:widgetLocation}(c), shows a widget is appended as a new leaf node (e.g. ads pop up in the current page). Case 3, Figure~\ref{fig:widgetLocation}(d), shows a deletion of irrelevant leaf node (e.g. settings to not display some UI widgets).

We design a recursive algorithm~\ref{algo:locateWidget} to precisely locate a UI widget. We represent the target widget as a series of hash value from root to the leaf. The algorithm accepts the current widget, recorded widget, the hash list of target widgets, and the current index of the hash list as parameters. When the index is equal to the size of the hash list, the function returns (Line 3). First, it checks whether the children of current widget contains the next hash value. If yes, the algorithm will recursively go down to check the corresponding child widget (Line 9). If not, it would exclude the intersection of the children of current widget and the recorded widget to narrow down the search space and try to recursively go down to check the unmatched child widget (Line 12-18). For Case 1, because substitution of non-leaf node would not change the hash of the node, so we can still locate the recorded widget. However, in Case 2, the hash value of the root changes after adding a new leaf node, which is equal to the sum of all leaf nodes. The algorithm will detect that one child of the current node contains the next hash value (Line 7) and finally locate the widget. As for Case 3, all of the hash values change before reaching the target widget after deleting a leaf node. The algorithm will fail to check the next recorded hash value in Line 7 and jump to Line 12. Then, the algorithm will filter two children of current view that can be mapped to the recorded view and choose the middle child to explore, where it can precisely find the recorded widget.

\begin{figure}[t!]
\centering
  \includegraphics[width=0.9\linewidth]{./picture/widgetLocationExample}
  %\vspace{-1em}
  \caption{Cases of locating widgets.}~\label{fig:widgetLocation}
  \vspace{-2em}
\end{figure}
\begin{algorithm}
\scriptsize
    \caption{Locating a specific UI element.}
    \label{algo:locateWidget}
    \SetAlgoLined
    \KwIn{Current Widget $cv$, Recorded Widget $rv$, Hash List $L$, Index $i$}
    \KwOut{Target Widget $tw$}
        \textbf{function}~LocateWidget($cv$, $rv$, $L$, $i$)\\
       \If{$i = L.size()$}{
            \Return{$cv.hash = L[i] ? cv : null$}
       }
        $children \leftarrow cv.children()$ \\
        $rv^{'} \leftarrow rv.children().get(L[i+1])$ \\

        \If{$children.has(L[i+1])$}{
        	$cv^{'} \leftarrow children.get(L[i+1])$ \\
        	$widget \leftarrow LocateWidget(cv^{'}, rv^{'}, L, i+1)$ \\
        }\Else{
          $children^{'} \leftarrow rv.children()$ \\
          \ForEach{$child \in (children - (children \cap children^{'}))$}{
              $widget \leftarrow LocateWidget(child, rv^{'}, L, i+1)$ \\
              \If{$widget != null$}{
                  $break$
              }
          }
        }
     \Return{$widget$}
\end{algorithm}
\vspace{-1em}

%Although the assigned hash value can distinguish nodes with different structure, its ability to tolerate app change is quite low. In the reproducing phase, we need a more adaptive approach to identify current state to address the flakiness issue.

%\begin{figure*}[t!]
%\centering
%  \includegraphics[width=0.9\textwidth]{./picture/tool}
%  \vspace{-1em}
%  \caption{Architecture of Paladin.}~\label{fig:workflow}
%  \vspace{-1em}
%\end{figure*}
