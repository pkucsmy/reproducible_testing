The key insight of Paladin is that the structure of the GUI view tree of a specific app page remains almost the same regardless of the changes of the contents and layouts when a test case is re-executed in different device configurations. Therefore, we use the complete structure of the GUI view tree to build a structure-preserving model for identifying equivalent app states and locating UI widgets precisely. We next describe the details of the fundamental design of equivalent state identification and UI widget location.

\subsection{State Equivalence}
In Android, all the UI widgets of an app page are organized in a GUI view tree, similar to the DOM tree of a web page. After the app page is rendered, the view tree can be retrieved via UI Automator~\cite{automator}, which is a tool provided by the Android SDK. Figure~\ref{fig:viewtree}(a) shows an app page which contains a text, a button, and an image button. Figure~\ref{fig:viewtree}(b) shows an excerpt of the retrieved view tree. We can see that the text and the button are organized in a linear layout, and the linear layout together with the image button are organized in another linear layout. Formally, a view tree is represented by a tuple $VT = <N, E>$. Here, $N$ is the set of view nodes, of which some are directly exhibited on the page (the text node, button node, and image button node) and the others are used to organize the layout (the two linear layout nodes).  $E\subset N\times N$ represents the parent-child relationship between nodes, where $e(n_1, n_2)\in E$ if $n_1$ is the parent node of $n_2$.

We propose to use the structure of the GUI view tree to identify equivalent app states. This design is inspired by the empirical fact that the view trees of GUIs produced by the same app behavior often share the similar structures, but different app behaviors typically result in different view trees. For example, the pages that show the details of different restaurants have the same structure, but the pages showing restaurant details and those showing a list of restaurant search results are obviously different in the structure of the view tree. Moreover, the rendered pages produced by a specific app behavior have similar view trees across different device configurations. Therefore, using the structure of the view tree to identify equivalent states can address the fragmentation issue.

\begin{figure}[t!]
\centering
  \includegraphics[width=1\linewidth]{./picture/viewtree}
  \vspace{-2em}
  \caption{An example of a view tree.}~\label{fig:viewtree}
  \vspace{-2em}
\end{figure}

In order to rapidly compare the view trees and identify the differences between two view trees, we encode each node of the view tree into a hash value and maintain a hash tree. The hash value of the root node can be used to distinguish app states. The hash function should meet two requirements. On one hand, the hash values should be different for view trees whose structures are different. On the other hand, given two view trees which have slightly differences, it should be efficient to find the differences in the structure based on the hash values. To meet these two requirements, we design a recursive bottom-up algorithm to compute the hash value of each node as shown in Algorithm~\ref{algo:structhash}. The algorithm accepts a view node $r$ as input. If $r$ has children nodes (Line 5), we use the algorithm to calculate all the hash values of its children recursively (Lines 7-9). Then, the algorithm sorts $r$'s children based on their hash values to ensure the consistency of the structure hash value (Line 10), since a view's children do not keep the same order every time. Later, we concatenate the children's hash as $r.hashString$, and return the hash value of $r.hashString$ (Lines 11-15). If $r$ does not have children nodes, the result is only the hash value of $r$'s view tag and resource ID (Line 18). Given the root node of the view tree, the algorithm returns a hash value, which can be used to identify equivalent states.

\begin{algorithm}
\scriptsize
    \SetAlgoLined
    \KwIn{View $r$}
    \KwOut{Structure Hash $h$}
        \textbf{function}~TreeHash($r$)\\
        %$r.hash \leftarrow 0$ \\
        \If{$r.InstanceOfWebview()$} {
                $r.setChildren \leftarrow r.parseHTML()$\\
        }
        \If{$r.hasChildren()$} {
            $children \leftarrow r.getChildren()$\\
            \ForEach{$c \in children$} {
                $c.hash \leftarrow TreeHash(c)$\\
            }
            $children \leftarrow SortByHash(r.getChildren())$\\
            $r.hashString \leftarrow ""$ \\
            \ForEach{$c \in children$} {
                $r.hashString \leftarrow r.hashString + c.hash$ \\
            }
            \Return{$hash(r.hashString)$} \\
        }\Else{
            \Return{$hash(r.viewTag + r.resourceId)$}
        }
    \caption{Computing the hash value of a view node.}\label{algo:structhash}
\end{algorithm}

%In many cases, there is a list-view or recycle-view that contain multiple views with the same structure. Under such circumstances, we should count the items with the same structure hash by only once (Line 13). Next, we add each children's hash together with the view tag, forming a new string (Line 16), and

This encoding method can be extended to Web elements as well. Since there is a plenty of hybrid apps which use HTML in WebView to display GUIs, we also incorporate the Web elements inside WebView component into the view tree (Line 3) rather than treating the WebView as a leaf node. So our model is applicable for hybrid apps.

Due to the non-determinism in app behaviors, the GUI may change in certain parts when the same sequence of events are triggered, and a trivial GUI change may result in a totally different hash value. For example, when a test case that explores the news page is re-executed, a notification about the news update may appear on the top of the screen. Since it is only a trivial GUI change and does not influence the majority of the other functionality, we should tolerate this difference and consider it to reach the expected state. Therefore, in order to make the model adaptive to changes, we design a structural similarity criteria. Given a computed GUI state triggered by an event, only when the similarity difference between the GUI's state and our stored state is above a threshold, we would regard it as a new state. Algorithm~\ref{algo:diff} shows how the similarity score is computed.

The algorithm accepts two view nodes and a threshold as inputs. If the two view nodes share the same hash value, their structures are the same, so the similarity equals 1 (Lines 2-4). Otherwise, there must be some differences between them. So we enumerate the children of these two nodes, and compute the similarities of the children nodes (Lines 9-17). To reduce the complexity, we will stop traversing if any pair of children nodes exceed the threshold (Lines 12-15). The similarity score is twice the number of nodes that are considered as the same divided by the total number of nodes including the two view nodes and all of their descendant nodes (Line 18).

%\begin{algorithm}
%\footnotesize
%    \caption{DFS Explore}
%    \label{algo:explore}
%    \SetAlgoLined
%    \KwIn{Current UI Tree $tree$, State Model $model$}
%        \textbf{function}~DFS\_Explore($A$, $model$)\\
%        \ForEach{$widget \in tree.getClickableWidget()$} {
%            $GenerateEvent(widget)$\\
%            $currentTree \leftarrow BuildTree()$\\
%            \If{$it is a UI state not yet explored$} {
%                $model.add(currentView)$\\
%                $DFS\_Explore(currentTree, model)$\\
%                $backTrack(tree, currentTree)$\\
%            }
%        }
%\end{algorithm}



%\begin{algorithm}
%\footnotesize
%    \caption{backTrack}
%    \label{algo:back}
%    \SetAlgoLined
%    \KwIn{Target State $T$, Current State $S$}
%        \textbf{function}~backTrack($T$, $S$)\\
%        $stack \leftarrow T.UIStack$\\
%        \If{$stack.activity \ne getCurrentActivity()$} {
%            $SendIntent(stack.intent)$\\
%        }
%        $S \leftarrow getCurrentState$\\
%        \ForEach{$state \in stack.states$} {
%            \If{$Similarity(S, state) > threshold$} {
%                $stack.ReplayTracesFrom(state)$\\
%            }
%        }
%\end{algorithm}

\begin{algorithm}
\scriptsize
    \SetAlgoLined
    \KwIn{View $s$, View $t$, threshold $\tau$}
    \KwOut{Structural Similarity $sim$}
        \textbf{function}~Similarity($s$, $t$)\\
        \If{$s.hash = t.hash$} {
            \Return{$1$}\\
        }
	   $hits \leftarrow 0$\\

	\If{$s.tag = t.tag$} {
            $hits \leftarrow hits + 1$
        }

        \ForEach{$sc \in s.getChildren()$} {
            \ForEach{$tc \in t.getChildren()$} {
                $tmp \leftarrow Similarity(sc, tc)$\\
                \If{$tmp > \tau$} {
                    $hits \leftarrow hits + tmp * tc.count$\\
                    $break$\\
                }
            }
        }
        \Return{$2 * hits / (s.count + c.count)$}
\caption{Structural similarity of two view trees.}\label{algo:diff}
\end{algorithm}
\vspace{-1em}

\subsection{Locating UI Widget}
To ensure the reproducibility of test cases, we also need to precisely locate the UI widgets to trigger the desired events during test-case execution. Under such a condition, when a generated test case is re-executed, each event can be triggered at the same UI widget. Based on the GUI model designed above, a UI widget is represented using the hash values from the root node to the node of the UI widget.
We summarize 3 cases in Figure~\ref{fig:widgetLocation} to illustrate the problems of locating UI widgets when the view tree of the same page is changed.
Consider an example view tree shown in Figure~\ref{fig:widgetLocation}(a). Assume that the leaf node with the dashed border line is to be located.
Figure~\ref{fig:widgetLocation}(b) shows Case 1, where there is a change on the black node (e.g. android.view.View in Android 5.1 is replaced by android.view.ViewGroup in Android 6.0+ instead).
Figure~\ref{fig:widgetLocation}(c) shows Case 2, where a widget is appended as a new leaf node (e.g. ads pop up in the current page).
Figure~\ref{fig:widgetLocation}(d) shows Case 3, where a deletion of irrelevant leaf node (e.g. settings to not display some UI widgets).
In all these three cases, the view tree is changed. As a result, the recorded hash values could not be used to locate the corresponding UI widgets.

We design a heuristic search algorithm to locate a UI widget. As shown in Algorithm~\ref{algo:locateWidget}, it accepts a widget of the current view tree, a widget of the recorded view tree, the hash list of the target widget to be matched, and the current index in the hash list as input. When the index is equal to the size of the hash list, the algorithm returns (Line 3). Otherwise, it first checks whether the children of the current widget contains the next hash value. If yes, the algorithm will recursively go down to check the corresponding child widget (Line 9). If not, it would exclude the intersection of the children of current widget and the recorded widget to narrow down the search space, and try to recursively go down to check the unmatched child widgets (Line 12-18). For Case 1, since a substitution of a non-leaf node would not change the hash value of the node, we can still locate the recorded widget. However, for Case 2, the hash value of the root node changes after adding a new leaf node, which is equal to the hash of concatenation of all the children's hash value. The algorithm will detect that one child of the current node contains the next hash value (Line 7) and finally locate the widget. As for Case 3, all of the hash values change before reaching the target widget after deleting a leaf node. The algorithm will fail to check the next recorded hash value in Line 7 and jump to Line 12. Then, the algorithm will filter two children of current view that can be mapped to the recorded view and choose the middle child to explore, where it can precisely find the recorded widget.

\begin{figure}[t!]
\centering
  \includegraphics[width=0.9\linewidth]{./picture/widgetLocationExample}
  %\vspace{-1em}
  \caption{Cases of locating widgets.}~\label{fig:widgetLocation}
  \vspace{-2em}
\end{figure}
\begin{algorithm}
\scriptsize
    \caption{Locating a specific UI widget.}
    \label{algo:locateWidget}
    \SetAlgoLined
    \KwIn{Current Widget $cv$, Recorded Widget $rv$, Hash List $L$, Index $i$}
    \KwOut{Target Widget $tw$}
        \textbf{function}~LocateWidget($cv$, $rv$, $L$, $i$)\\
       \If{$i = L.size()$}{
            \Return{$cv.hash = L[i] ? cv : null$}
       }
        $children \leftarrow cv.children()$ \\
        $rv^{'} \leftarrow rv.children().get(L[i+1])$ \\

        \If{$children.has(L[i+1])$}{
        	$cv^{'} \leftarrow children.get(L[i+1])$ \\
        	$widget \leftarrow LocateWidget(cv^{'}, rv^{'}, L, i+1)$ \\
        }\Else{
          $children^{'} \leftarrow rv.children()$ \\
          \ForEach{$child \in (children - (children \cap children^{'}))$}{
              $widget \leftarrow LocateWidget(child, rv^{'}, L, i+1)$ \\
              \If{$widget != null$}{
                  $break$
              }
          }
        }
     \Return{$widget$}
\end{algorithm}
\vspace{-1em}

%Although the assigned hash value can distinguish nodes with different structure, its ability to tolerate app change is quite low. In the reproducing phase, we need a more adaptive approach to identify current state to address the flakiness issue.

%\begin{figure*}[t!]
%\centering
%  \includegraphics[width=0.9\textwidth]{./picture/tool}
%  \vspace{-1em}
%  \caption{Architecture of Paladin.}~\label{fig:workflow}
%  \vspace{-1em}
%\end{figure*}
