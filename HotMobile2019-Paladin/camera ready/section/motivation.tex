\begin{table}[t!]
\centering
\caption{Characteristics study of automated-test-generation and record-and-replay tools.}
\vspace{-1em}
\label{tab:compare_tool}
\footnotesize
\begin{tabular}{ccc}
\hline
\textbf{Tools}      &   \textbf{State Equivalence}
					&   \textbf{UI Widget Location}    \\ \hline
\multicolumn{3}{c}{\textbf{Automated-Test-Generation (ATG) Tools}}                                                                                                                                                                        \\ \hline
AppDoctor           & GUI Content           & Coordinates           \\
Collider            & Event Handler           & N/A                     \\
CrashScope          & GUI Content         & Coordinates            \\
JPF-droid           & Method Invocation          & Resource ID, Name, Label               \\
PUMA                & GUI Feature Vector           & Coordinates                            \\
Droidbot            & GUI Content             & Coordinates                            \\
Stoat               & \begin{tabular}[c]{@{}c@{}}String Encoding \\of View Tree\end{tabular}           & Widget Indexes                          \\ \hline
\multicolumn{3}{c}{\textbf{Record-and-Replay Tools}}                                                                                                                                                                   \\ \hline
RERAN               & N/A            & Coordinates                      \\
VELERA              & Visual Perception             & Coordinates                \\
Mosaic              & N/A                & Coordinates, Scale                       \\
Barista             & Widget Existence            & \begin{tabular}[c]{@{}c@{}}Resource ID, XPath\end{tabular}     \\
ODBR                & N/A             & Coordinates                 \\
SPAG-C              & Image Comparison            & Coordinates             \\
MonkeyRunner        & N/A             & Coordinates                   \\
Culebra             & GUI Content            & \begin{tabular}[c]{@{}c@{}}Resource ID, Text, \\ Content Description\end{tabular}    \\ \hline
\end{tabular}
\end{table}


The execution of an Android app is driven by phone events, such as button clicks and SMS arriving. Thus, a test case for app analysis is represented as a sequence of phone events, where each phone event triggers a transition of app states. In this paper, we consider only the UI events such as click and scroll, which are triggered explicitly by user interactions. System events such as message notifications and network connections are left for future work. Besides, we take into account the GUI states, which focus on the UI layouts and widgets that are perceivable by users. We define that a test case of an app is reproducible if the app could perform consistent behaviors to reach the same states when the test case is re-executed in different device configurations, including the original device configuration where the test case is generated and other device configurations. Here, we regard the device configuration as the device model and Android version.

Based on the above definitions, reproducibility can be achieved if all the events in a test case are triggered in an expected order and produce the desired app states.
This brings two problems to be solved, state equivalence and widget location.
On one hand, how to identify equivalent app states should be designed carefully. Considering only the UI contents is likely to cause state space explosion, and considering only the page classes (i.e., activities on Android) is not enough to reproduce the desired behaviors. More importantly, the changes caused by the backend-service dependencies and the Android fragmentation issue must be considered for driving the app to a desired UI state and interacting with the desired UI widgets such as buttons.
%
%the state identification should be tolerant to the GUI changes caused by the non-determinism of behaviors in the target app. On the other hand, since the UI events are bound to certain widgets, there should be a way of uniquely locating widgets that could tolerate the changes and differences of GUI.

%Moreover, due to the challenges
%
%



%As is discussed before, the fundamental task for model extraction is to identify a state precisely. Therefore, a novel state identification rule should be designed carefully to strike a balance between accuracy and compactness.  Moreover, state identification should also be flexible to tolerate insignificant GUI changes during replay stage. Otherwise the generated test scripts will be severely subject to nondeterminism in the target app.

%Another question is how to accurately identify the UI element associated with actions. In a typical abstracted GUI transition graph, states represent various pages and edges represent transitions between states. For mobile apps, a large proportion of transitions are composed of actions performed on UI elements. Therefore, an appropriate approach to locate UI elements is essential for execution of generated test scripts.

%Before detailing our design to address the above challenges, it is important to take a step back, and analyze the support of reproducible test cases in research and practice.
Before presenting our design, we first make a characteristic study to investigate the current support of generating reproducible test cases. Based on a recent survey on Android testing~\cite{Mario2017ICSME}, we select 7 ATG tools of which the generated test cases are reported to be reproducible. We also select 8 record-and-replay tools from the same survey to study whether they can be integrated with the existing ATG tools to achieve reproducibility by using the generated test case as the recorded script. We focus on the state equivalence and the UI widget location adopted by these tools. Table~\ref{tab:compare_tool} summarizes the analysis results.

\subsection{State Equivalence}
%\noindent \textbf{Identifying App States.}
Most ATG tools consider two app states as equivalent ones if they have identical GUI contents. Obviously, such a strict definition would easily cause state explosion on today's complicated apps. For example, due to the non-deterministic behaviors caused by backend-service dependencies, the UI content varies slightly (e.g., pop-up ads on side bar) even when the same sequence of events are triggered, producing a daunting number of states for one page. Collider defines a state as a combination of registered event handlers and transitions as execution of event handlers. JPF-droid examines method invocations to verify states. Theoretically, method or handler invocations are more suitable than GUI changes to detect equivalent states for app exploration. But it requires instrumentation on the source code or byte-code level to emit events that indicate changes in the program state, which is difficult for complicated commercial apps. PUMA constructs a GUI feature vector and uses the cosine-similarity metric with a user-specified similarity threshold to identify equivalent states. Stoat encodes the string of the GUI view tree as a hash value to distinguish states. The problem is that the feature vector and the string encoding cannot easily adapt to different device configurations.

4 out of 8 record-and-replay tools do not specify state equivalence precisely. VELERA uses human visual perception to judge the test results, which is impractical for large-scale analysis. Culebra uses the GUI content to identify equivalent states, suffering from state explosion as discussed before. SPAG-C uses image comparison to identify equivalent states. However, image comparison is very susceptible to slight GUI changes such as different font styles and color settings.

\subsection{UI Widget Location}
%\noindent \textbf{Locating UI Widgets.}
There are 10 out of the total 15 examined tools that use the coordinates to locate widgets. However, considering only the absolute position of UI widgets is incapable of reusing test scripts on the devices with different screen resolutions. It is also error-prone for the GUI changes caused by different responses from backend services. Mosaic uses a series of scalings and normalizations to map coordinates between platforms. However, UI widgets do not simply scale linearly with screen dimensions. Some apps rearrange and even hide UI widgets based on the screen resolution. JPF-droid and Culebra take resource ID, label texts, and content descriptions into consideration. But UI widgets often share these properties, making it difficult to precisely locate the desired widgets. Barista uses XPath selector to locate UI widgets since the GUI view tree can be mapped to an XML document. But widget classes that constitute the XPath tags may differ in different Android versions. Stoat locates UI widgets by object indexes, which are likely to change under different device configurations.

In summary, test cases generated by the state-of-the-art tools are limited in achieving reproducibility even if the generated test cases can be used as the recorded scripts for record-and-replay tools.
